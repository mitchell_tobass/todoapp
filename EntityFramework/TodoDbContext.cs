﻿using Microsoft.EntityFrameworkCore;

public class TodoDbContext : DbContext
{
    public DbSet<TodoItem> Todos { get; set; } = default!;

    public TodoDbContext(DbContextOptions<TodoDbContext> options) : base(options)
    {

    }
}
