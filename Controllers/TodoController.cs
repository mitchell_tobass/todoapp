﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;
using Plain.RabbitMQ;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace TodoApplication.Controllers;

public class TodosController : ODataController
{
    private readonly TodoDbContext _db;
    private readonly IPublisher _publisher;

    public TodosController(TodoDbContext dbContext, IPublisher publisher)
    {
        _db = dbContext;
        _publisher = publisher;

        _db.Database.EnsureCreated();
    }

    [EnableQuery]
    public IQueryable<TodoItem> Get()
    {
        return _db.Todos;
    }

    [EnableQuery]
    public SingleResult<TodoItem> Get([FromODataUri] int key)
    {
        var result = _db.Todos.Where(c => c.Id == key);
        return SingleResult.Create(result);
    }

    [EnableQuery]
    public async Task<IActionResult> Post([FromBody] TodoItem todo)
    {
        _db.Todos.Add(todo);
        await _db.SaveChangesAsync();
        _publisher.Publish(JsonSerializer.Serialize(todo), "todo.created", null);
        return Created(todo);
    }

    [EnableQuery]
    public async Task<IActionResult> Patch([FromODataUri] int key, Delta<TodoItem> todo)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }
        var existingNote = await _db.Todos.FindAsync(key);
        if (existingNote == null)
        {
            return NotFound();
        }

        todo.Patch(existingNote);
        _publisher.Publish(JsonSerializer.Serialize(todo), "todo.updated", null);
        try
        {
            await _db.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!_db.Todos.Any(i => i.Id == key))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return Updated(existingNote);
    }

    [EnableQuery]
    public async Task<IActionResult> Delete([FromODataUri] int key)
    {
        var todo = await _db.Todos.FindAsync(key);
        if (todo == null)
        {
            return NotFound();
        }

        _db.Todos.Remove(todo);
        await _db.SaveChangesAsync();
        _publisher.Publish(JsonSerializer.Serialize(todo), "todo.removed", null);
        return StatusCode(StatusCodes.Status204NoContent);
    }
}
