﻿using System;
using System.ComponentModel.DataAnnotations;

public class TodoItem
{
    public int Id { get; set; }
[Required]
    public string Note { get; set; } = default!;
    public DateTime DueDate { get; set; }
}
